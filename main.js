const WebSocket = require('ws');

const host = 'wss://hdx-marketdata-websocket.prod.hashdex.io/v1';

var message = {
  type: 'subscribe',
  subscriptions: [
    {
      'type': 'index',
      'symbol': 'HDAI'
    },
  ]
}

console.log(`Connecting to websocket: ${host}`)
const ws = new WebSocket(host, { perMessageDeflate: false });

ws.on('open', function open() {
  console.log('Connected.');
  console.log(`subscribing with message: ${JSON.stringify(message)}`)
  ws.send(JSON.stringify(message))
});

ws.on('message', function incoming(data) {
  console.log('Message received.')
  console.log(data);
});

ws.on('close', function open() {
  console.log('disconnected.');
});