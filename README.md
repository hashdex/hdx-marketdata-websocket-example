# hdx-marketdata-websockets-example

## Como executar o exemplo

```bash
npm install
node src/main.js
```

## API Endpoint:

Documentation:
https://hdx-marketdata-websocket-docs.prod.hashdex.io/docs

Websocket endpoint:
https://hdx-marketdata-websocket.prod.hashdex.io/v1

## Subscriptions

Para criar uma subscription e receber dados em tempos reais é necessário enviar uma mensagem para o servidor com a seguinte estrutura:

```javascript
var message = {
  type: 'subscribe',
  subscriptions: [
    {
      'type': 'value',
      'symbol': 'value'
    },
    {
      'type': 'value',
      'symbol': 'value'
    },
    ...
  ]
}
```

Atualmente a única subscription ofertada é para o índice HDAI, isso pode ser feito com a mensagem abaixo:

```javascript
var message = {
  type: 'subscribe',
  subscriptions: [
    {
      'type': 'index',
      'symbol': 'HDAI'
    }
  ]
}
```

## Exemplo (NodeJS)

```bash
npm install ws --save
```

```javascript
const WebSocket = require('ws');

const ws = new WebSocket('https://hdx-marketdata-websocket.prod.hashdex.io/v1', { perMessageDeflate: false });

ws.on('open', function open() {
  ws.send(JSON.stringify(message))
});

ws.on('message', function incoming(data) {
  console.log('Message received.')
  console.log(data);
});
```
